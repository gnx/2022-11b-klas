package exceptions;

import java.util.Scanner;

public class Hero {
	int posx;
	int posy;
	
	Hero() throws Exception {
		System.out.println("Въведете координатите на героя.");
		
		Scanner scan = new Scanner(System.in);
		int x = scan.nextInt();
		int y = scan.nextInt();
		scan.close();
		
		if (x < 0) {
			throw new Exception();
		}
		
		if (y < 0) {
			throw new Exception();
		}
		
		posx = x;
		posy = y;
	}
	
	Hero(int x, int y) {
		posx = x;
		posy = y;
	}
	
	public String toString() {
		return posx + " " + posy;
	}
}
