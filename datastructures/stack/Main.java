package datastructures.stack;

import java.util.Scanner;
import java.util.Stack;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String input = scan.next();
		scan.close();
		
		Stack<Character> st = new Stack<>();
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c == '(') {
				st.push(c);
			} else if (!st.isEmpty()){
				st.pop();
			}
		}
		
		if (!st.isEmpty()) {
			System.out.println("Ainda temos " + st.size() + " assunto(s) pendente(s)!");
		} else {
			System.out.println("Partiu RU!");
		}
	}
}
