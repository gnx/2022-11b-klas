package datastructures.list;

public class VaflaArrayList {
	private int size = 0;
	Vafla[] arr = new Vafla[5];
	
	void add(Vafla v) {
		if (size < arr.length) {
			arr[size] = v;
			size++;
		} else {
			Vafla[] temp = new Vafla[arr.length + 1];
			for (int i = 0; i < arr.length; i++) {
				temp[i] = arr[i];
			}
			arr = temp;

			arr[size] = v;
			size++;
		}
	}
}
