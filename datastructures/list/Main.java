package datastructures.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String s = scan.nextLine();
		Queue<String> west = new LinkedList<String>();
		Queue<String> north = new LinkedList<String>();
		Queue<String> south = new LinkedList<String>();
		Queue<String> east = new LinkedList<String>();
		Queue<String> combined = new LinkedList<String>();
		Queue<String> current = null;
		while(!s.equals("0")) {
			if (s.equals("-4")) {
				current = east;
			} else if (s.equals("-3")) {
				current = north;
			} else if (s.equals("-2")) {
				current = south;
			} else if (s.equals("-1")) {
				current = west;
			} else if (s.equals("0")) {
				break;
			} else {
				current.add(s);
			}
			s = scan.nextLine();
		}
		
		while(!west.isEmpty() || !north.isEmpty() || !south.isEmpty() || !east.isEmpty()) {
			if (!west.isEmpty()) {
				combined.add(west.poll());
			}
			if (!north.isEmpty()) {
				combined.add(north.poll());
			}
			if (!south.isEmpty()) {
				combined.add(south.poll());
			}
			if (!east.isEmpty()) {
				combined.add(east.poll());
			}
		}
		
		int n = combined.size();
		for (int i = 0; i < n - 1; i++) {
			System.out.print(combined.poll() + " ");
		}
		System.out.println(combined.poll());
	}
}
