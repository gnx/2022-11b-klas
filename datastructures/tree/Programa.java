package datastructures.tree;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Programa extends Object {
	public static void main(String[] args) {
		Set<Attacks> tree = new TreeSet<>();
		tree.add(Attacks.FIREBALL);
		tree.add(Attacks.SHOOT);
		tree.add(Attacks.SHOOT);
		tree.add(Attacks.SHOOT);
		tree.add(Attacks.FIREBALL);
		System.out.println(tree);
		
		Set<Integer> hash = new HashSet<>();
		hash.add(345);
		hash.add(9);
		hash.add(3);
		hash.add(12);
		hash.add(9);
		hash.add(9);
		hash.add(1);
		hash.remove(3);
		System.out.println(hash);
	}
}