package zoo;

public abstract class Bird extends Animal {
	static final int wings = 2;
	
	Bird() {
		super();
	}
	
	Bird(String name) {
		super(name);
		legs = 2;
	}
}
