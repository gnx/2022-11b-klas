package zoo;
public final class Giraffe extends Mammal implements Trevopasno {
	double neckLength;
	
	void printInfo() {
		System.out.println(color);
		System.out.println(name);
		System.out.println(weight);
		System.out.println(legs);
		System.out.println(neckLength);
	}

	@Override
	public void paseTreva() {
		System.out.println("Аз съм жираф и паса трева");
	}

	@Override
	public void prezhivq() {
		System.out.println("Аз съм жираф и преживям");
	}
}