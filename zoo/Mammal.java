package zoo;
public abstract class Mammal extends Animal {
	Mammal() {
		super();
		legs = 4;
	}
}
