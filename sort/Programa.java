package sort;

import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {
		// създаване
//		Scanner scan = new Scanner(System.in);
		int[] a = {9, -8, 2, 81, 7};
		
		// сортиране
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = 1; j < a.length - i; j++) {
				if (a[j - 1] > a[j]) {
					a[j - 1] += a[j];
					a[j] = a[j - 1] - a[j];
					a[j - 1] -= a[j];
				}
			}
		}
		
		// извеждане
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}
}
